import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'tutorial-webcomponents-angular';

  usertype = 'customer';
  userTypeChange(val){
    this.usertype = val;
  }

  ngOnInit(){
    const component = document.querySelector('spa-component');
    component.addEventListener('my-event', (e) => {
      console.group('console from host')
      console.info('Event name: my-event')
      console.log('Event string: ' + e['detail']) 
      console.groupEnd()
    });
  }
}
